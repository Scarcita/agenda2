import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http : HttpClient) { }
  
  postClientes(data : any){
    return this.http.post<any>('http://localhost:3000/listaClientes/',data);
  }
  putClientes(data: any, id : number){
    return this.http.put<any>('http://localhost:3000/listaClientes/'+id, data);
  }

  getClientes(){
    return this.http.get<any>('http://localhost:3000/listaClientes/');
  }
  deleteClientes(id: number){
    return this.http.delete<any>('http://localhost:3000/listaClientes/'+id);
  }
}
