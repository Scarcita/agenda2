import { Component, inject, OnInit } from '@angular/core';
import {FormControl, FormGroup, FormBuilder, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { ApiService } from '../services/api.service';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';




export class MyErrorStateMatcher implements ErrorStateMatcher {

  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): 
  boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-dialogo',
  templateUrl: './dialogo.component.html',
  styleUrls: ['./dialogo.component.css']
})
export class DialogoComponent implements OnInit{

  

  emailFormControl = new FormControl('', [Validators.required, Validators.email]);

  matcher = new MyErrorStateMatcher();

  productForm!: FormGroup;
  actionBtn : string = "save"

  constructor(private formBuilder : FormBuilder,
              private api: ApiService,
              @inject(MAT_DIALOG_DATA) public editData: any,
              private dialogRef : MatDialogRef<DialogoComponent>) { }

  ngOnInit(): void {
    this.productForm = this.formBuilder.group({
      Nombre : ['', Validators.required],
      Apellidos : ['', Validators.required],
      Email : ['', Validators.required],
      Celular : ['', Validators.required],
      Hora : ['', Validators.required],
      Servicio : ['', Validators.required],
      Fecha : ['', Validators.required],
      Descripcion : ['', Validators.required],
    });

    if (this.editData) {
     this.actionBtn = "update"
     this.productForm.controls['Nombre'].setValue(this.editData.Nombre)
     this.productForm.controls['Apellidos'].setValue(this.editData.Apellidos)
     this.productForm.controls['Email'].setValue(this.editData.Email)
     this.productForm.controls['Celular'].setValue(this.editData.Celular)
     this.productForm.controls['Hora'].setValue(this.editData.Hora)
     this.productForm.controls['Servicio'].setValue(this.editData.Servicio)
     this.productForm.controls['Fecha'].setValue(this.editData.Fecha)
     this.productForm.controls['Descripcion'].setValue(this.editData.Descripcion)
    
   }
  }
  


   enviarCita() {
     if(!this.editData) {
       if(this.productForm.valid){
       }
       }else {
         this.updateClientes()
       }
     }
     updateClientes(){
       this.api.putClientes(this.productForm.value, this.editData.id)
       .subscribe({
         next:(res)=> {
           alert('Cita Actualizada con exito');
           this.productForm.reset();
           this.dialogRef.close('update')
         },
            error:() =>{
              alert("Error al actualizar");
            } 
      })

    }
    }
 
